#include <thread>

#include "ros/ros.h"
#include <kortex_driver/Base_ClearFaults.h>
#include <kortex_driver/PlayCartesianTrajectory.h>
#include <kortex_driver/CartesianSpeed.h>
#include <kortex_driver/BaseCyclic_Feedback.h>
#include <kortex_driver/ReadAction.h>
#include <kortex_driver/ExecuteAction.h>
#include <kortex_driver/PlayJointTrajectory.h>
#include <kortex_driver/SetCartesianReferenceFrame.h>
#include <kortex_driver/CartesianReferenceFrame.h>
#include <kortex_driver/SendGripperCommand.h>
#include <kortex_driver/GripperMode.h>

#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <iostream>
#include <fstream>
#include <string> 
using namespace std;



/**
 * This is a code for the subscriber node, that writes down the kinova statistics into a textfile.
 * I based this on the following tutorials:
 * 
 * https://wiki.ros.org/ROS/Tutorials/UnderstandingTopics * 
 * https://wiki.ros.org/roscpp/Overview/Publishers%20and%20Subscribers
 * https://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29
 * https://wiki.ros.org/roscpp/Overview/Initialization%20and%20Shutdown
 */
 
 ofstream file;
 int counter=0;
 string buffer="Position Current \n";


/**
 * Our own Sigint, when the Ctrl+C is pressed
 * what's left in the buffer is saved and the filestream is closed
 * then the ros shutdown is called
 */ 
void mySigintHandler(int sig)
{
  /** Do some custom action.
  * In my case save and close the file
  */
  file << buffer;
  file.close();
  // All the default sigint handler does is call shutdown()
    
  ros::shutdown();
}
 
/**
 * This is the callback function that will get called when a new message 
 * has arrived on our desired topic.
 * The ROS INFO prints out more gripper motor stats than we currently need, 
 * but only the position and current will be saved into the text file. It
 * is filling up a buffer that is emptied every 20th message.
 * 
 */
void feedbackCallback(const kortex_driver::BaseCyclic_Feedback::ConstPtr& feedback)
{
  counter++;
  
  /** Getting the stats we want   */ 
  float actual_gripper_position = feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].position/ 100.0;
  float actual_gripper_current = feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].current_motor;
  float actual_gripper_voltage = feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].voltage;
  float actual_gripper_temp = feedback->interconnect.oneof_tool_feedback.gripper_feedback[0].motor[0].temperature_motor;
  
  ROS_INFO("Position: %f Current: %f Voltage: %f Temp: %f",actual_gripper_position,actual_gripper_current,actual_gripper_voltage,actual_gripper_temp);
  buffer+=to_string(actual_gripper_position)+" "+to_string(actual_gripper_current)+"\n";
  
  if(counter==20){
	file << buffer;
	buffer="";
	counter=0;
  }
  
}

int main(int argc, char **argv)
{      
	/**
	 * Initializing the ROS
	 * and setting up the node handler
	 */ 
	ros::init(argc, argv, "Kinova_feedback_subscriber",ros::init_options::NoSigintHandler);
	ros::NodeHandle n;
	
	
	/** Reading the current time so we can name the textfile accordingly. 	 */
	time_t now = time(0);
	tm *ltm = localtime(&now);	
	string filename="Kinova-"+to_string(1900 + ltm->tm_year)+"-";
				
	if(1+ltm->tm_mon<10){
		filename+="0";
	}
	filename+=to_string(1 + ltm->tm_mon)+"-";
	if(ltm->tm_mday<10){
		filename+="0";
	}
	filename+=to_string(ltm->tm_mday)+"-";
	if(ltm->tm_hour<10){
		filename+="0";
	}
	filename+=to_string(ltm->tm_hour)+"-";
	if(ltm->tm_min<10){
		filename+="0";
	}
	filename+=to_string(ltm->tm_min);

	// Opening the filestream
	file.open("KinovaLogs/"+filename+".txt",ios::in | ios::trunc);

	//Checking if the process was successful
	if (file.is_open()){
		ROS_INFO("Output operation successfully performed\n");
	}else{
		ROS_INFO("Error opening file");
		ros::shutdown();
		return 1;
	}   

	

	/** Subscribing to the topic	 */
	ros::Subscriber sub = n.subscribe("/my_gen3/base_feedback", 1000, feedbackCallback);
	
	/** setting the sigint handler  */
	signal(SIGINT, mySigintHandler);
	ros::spin();
	return 0;
}
