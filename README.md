# ipalm_control
Check out [MagicCommands](https://gitlab.fel.cvut.cz/hartvjir/ipalm_control/-/blob/master/MagicCommands.md) for a summary of essential commands at the kinova workstation.<br><br>
<b>Caveats</b>
*  <b>!!!</b> If the camera can't see the marker then the broadcaster from reference_point.py says errors as if it were incorrectly used<br>
*  If the quaternion is not normalized then ROS complains and the broadcaster may show errors.<br>
*  The variable camera_locations is <b>not</b> indexable, using enumerate to go around it
<br>
<br>
<br>
<br>
<br>
