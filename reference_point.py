#!/usr/bin/env python

from __future__ import print_function

import os
import sys

import cv2
from cv2 import aruco
import numpy as np
import pyrealsense2 as rs
import tf
import rospy, roslib
import geometry_msgs
import tf2_msgs
import json
import math

import multi_camera_setup
CAMERAS = ["013222072095", "013222072762"]

ROBOT_POI = [0, 0, 0]  # TODO: SET THE CORRECT ROBOT POSITION in m
CAMERA_POI = [[1.14,0.010,0.0265],[1.14,-0.010,0.0265]] #camera position in base reference measured roughly

# Correct AruCo marker needs to be used, by default the Classic AruCo markers are used
ARUCO_MARKER_ID = 25
DISTANCE_MARKER_ID = 219
MEASURING = False


def get_distance(x, y, depth_img, scale):
    x_size = depth_img.shape[0]
    y_size = depth_img.shape[1]
    if x >= x_size:
        x = x_size - 1
    if y >= y_size:
        y = y_size - 1
    # Needs to be switched for RS have different coord systems
    return depth_img[x, y] * scale

def calculate_3d_distance(point1, point2):
    dist = math.sqrt(
        (point1[0] - point2[0])**2 + (point1[1] - point2[1])**2 + (point1[2] - point2[2])**2)
    return float("{0:.2f}".format(dist))

def mean(lst):
    return sum(lst) / len(lst)

def weighted_average(lst):
    weighted = 0
    for i, val in enumerate(lst):
        weighted += val * (i + 1)
    divisor = sum(range(1, len(lst)+1))
    return weighted / divisor


def broadcast_tf(translation, rotation, target, parent='/base_link'): # rotation is quaternion x,y,z,w
    br = tf.TransformBroadcaster()
    br.sendTransform(translation,
                     rotation,
                     rospy.Time.now(),  # also broadcasts rostime
                     target,
                     parent)

def broadcast_combined_tf(camera_location, target='/camera_link', parent='/base_link'):
    broadcast_tf(camera_location[0][0],
                     tf.transformations.quaternion_from_euler(camera_location[1][0][0],
                                                              camera_location[1][0][1],
                                                              camera_location[1][0][2]),
                     target,
                     parent) 

def create_camera_message(camera_location, time, child, parent):

    t = geometry_msgs.msg.TransformStamped()
    t.header.frame_id = parent
    t.header.stamp = time
    t.child_frame_id = child
    t.transform.translation = camera_location[0][0]

    t.transform.rotation = tf.transformations.quaternion_from_euler(camera_location[1][0][0],
                                                                    camera_location[1][0][1],
                                                                    camera_location[1][0][2])
    tfm = tf2_msgs.msg.TFMessage([t])
    return tfm

if __name__ == '__main__':
    cameras = multi_camera_setup.camera_setup(CAMERAS)


    while True:
        output_images = []
        camera_locations = []
        for camera in cameras:
            (camera_id, align, depth_scale,
             pc, points, pipeline, profile) = camera
            # Get frameset of color and depth
            frames = pipeline.wait_for_frames()

            # Align the depth frame to color frame
            aligned_frames = align.process(frames)

            # Get aligned frames
            depth_frame = aligned_frames.get_depth_frame()
            color_frame = aligned_frames.get_color_frame()

            depth_intrin = depth_frame.profile.as_video_stream_profile().intrinsics

            # Tell pointcloud object to map to this color frame
            pc.map_to(color_frame)

            # Generate the pointcloud and texture mappings
            points = pc.calculate(depth_frame)

            # Validate that both frames are valid
            if not depth_frame or not color_frame:
                continue

            color_image = np.asanyarray(color_frame.get_data())
            depth_image = np.asanyarray(depth_frame.get_data())

            color_intrin = color_frame.profile.as_video_stream_profile().intrinsics
            camera_matrix = np.array(
                [[color_intrin.fx, 0, color_intrin.ppx],
                 [0, color_intrin.fy, color_intrin.ppy],
                 [0, 0, 1]], dtype="double"
            )
            dist_coeff = np.array(color_intrin.coeffs)

            aruco_dict = aruco.Dictionary_get(aruco.DICT_ARUCO_ORIGINAL)
            parameters = aruco.DetectorParameters_create()
            corners, ids, rejectedImgPoints = aruco.detectMarkers(color_image, aruco_dict, parameters=parameters)
            if corners:

                if ARUCO_MARKER_ID in ids:
                    marker_idx = np.where(ids == ARUCO_MARKER_ID)[0]
                    marker_idx = marker_idx[0]
                    # # 2D points
                    # image_points = np.array([tuple(corners[0][0, 0])], dtype="double")
                    #
                    # # 3D model points.
                    # model_points = np.array([
                    #                         (0.0, 0.0, 0.0)
                    #                         ], dtype="double")
                    rvecs, tvecs, obj_pts = aruco.estimatePoseSingleMarkers(corners, 0.1, camera_matrix, np.array(color_intrin.coeffs))
                    position = tvecs[marker_idx]
                    rotation = rvecs[marker_idx]
                    camera_locations.append((position, rotation))
                    if MEASURING and DISTANCE_MARKER_ID in ids:
                        dist_marker_idx = np.where(ids == DISTANCE_MARKER_ID)[0]
                        dist_marker_idx = dist_marker_idx[0]
                        dist_position = tvecs[dist_marker_idx]
                        print(position[0][0])
                        print(dist_position[0][0])
                        marker_dist = calculate_3d_distance(position[0], dist_position[0])
                        print('found distance {}'.format(marker_dist))

                        rotation_matrix, jacobi = cv2.Rodrigues(np.array(rotation))
                        transformation_matrix = np.array([[0, 0, 0, position[0][0]],
                                                          [0, 0, 0, position[0][1]],
                                                          [0, 0, 0, position[0][2]],
                                                          [0, 0, 0, 1]])
                        transformation_matrix[:-1, :-1] = rotation_matrix

                        inverted_matrix = np.linalg.inv(transformation_matrix)

                        poi = list(dist_position[0])
                        poi.append(1)
                        poi_camera = np.array(poi)
                        poi_camera.shape = (4, 1)
                        poi_robotframe = np.matmul(inverted_matrix, poi_camera)
                        print(poi_robotframe)


            color_image = aruco.drawDetectedMarkers(color_image, corners, ids)
            output_images.append(color_image)

        images = np.hstack(output_images)


        #rospy.Subscriber('/base_link/camera_link1', tf2_msgs.msg.TFMessage ,publishing_values) 
        
        cv2.namedWindow('Detected camera view', cv2.WINDOW_AUTOSIZE)
        cv2.putText(images, "q: exit, c: save positions, p: print positions",
                (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 2)
        cv2.imshow('Detected camera view', images)
        key = cv2.waitKey(1)
        # Press esc or 'q' to close the image window
        if key & 0xFF == ord('q') or key == 27:
            cv2.destroyAllWindows()
            break
        if key & 0xFF == ord('c'):
            print('Saving position of marker.')
            np.save('position', camera_locations)
            for i, camera in enumerate(camera_locations):
                print('camera {}'.format(i), camera)
        if key & 0xFF == ord('p'):
            print('Saved position:\n', str(np.load('position.npy')))
            print('Last measured position:\n', str(position))
            print()

        if key & 0xFF == ord('a'):
            print('Corners:\n' , str(corners))


        rospy.init_node('camera_broadcaster')

        # The lines below not necessary 
        #listener = tf.TransformListener()
        #publisher = rospy.Publisher('/camera_link1', tf2_msgs.msg.TFMessage , queue_size=100)
        #rate = rospy.Rate(10.0)
        ''' 
        # example of working publisher but redundant in this case because it does the same job as broadcasting to tf
        for location in camera_locations:
            broadcast_location(location)
            #print("Broadcasted location")
            #import ipdb; ipdb.set_trace()                
            cam_msg = create_camera_message(location, rospy.Time(0), '/camera_link1', '/base_link')
            #print("Published location: ", cam_msg)
            publisher.publish(cam_msg)
        '''


        #import ipdb; ipdb.set_trace()  # use for step by step debugging
        for i, location in enumerate(camera_locations):
            broadcast_combined_tf(location, '/mark','/camera{}'.format(i)) # rosrun tf tf_echo /mark /camera1

        camera_tfs = [ [CAMERA_POI[0], [0.674, 0.153, -0.654, -0.308]], # TODO this is a random number and probably wrong, please check how the quaternions work
                       [CAMERA_POI[1], [0.674, 0.153, -0.654, -0.308]] ] # naively measured for now
        
        for i, transrot in enumerate(camera_tfs):
            broadcast_tf(transrot[0], transrot[1], 'camera{}'.format(i), '/base_link') # rosrun tf tf_echo /mark /camera1
            # TODO ? find out why in RVIZ one mark going from the cameras seems to be glitching

'''        try:
            for location in camera_locations:
                #print(location) # location[0][0] = translation, location[1][0] = rotation

                broadcast_location(location)
                
                #print("Broadcasted location")
                cam_msg = create_camera_message(location, rospy.Time(0), '/camera_link1', '/base_link')
                #print("Published location: ", cam_msg)
                publisher.publish(cam_msg)
        except:
            #continue
            print("Broadcast .")
        
        """
        try:
            (trans, rot) = listener.lookupTransform('/base_link','/camera_link1', rospy.Time(0))
            #print("00000")
            #print(trans)
        except:
            pass
        """
'''


