## Terminator
`ctrl + shift + w` = close<br>
`ctrl + shift + e` = open tab on side<br>
`ctrl + shift + o` = open tab on bottom<br>
`alt + arrows` = move between underterminals<br>
<br>
## ROS commands
`rosservice list | grep whatever` - e.g. `gripper`, get services with gripper. Same for `rostopic list` and `rosparam list`.<br>
`rosservice call /manipulator/...` - or `/my_gen3/...` or `/kinova_gen3/...`, calls service<br>
`roslaunch kortex_driver kortex_driver.launch start_rviz:=false` - starts kortex_driver, needs to be done <i>before</i> we can launch other things<br>
`roscore` - is the bare minimum that can be run of ROS<br>
`rostopic info /tf` - gives us info about the topic /tf, transforms containing translation and quaternions<br>
`rostopic echo /tf` - prints everything the topic /tf has to offer, warning: spam<br>
`rosrun tf tf_echo /parent_directory /child_directory` - replace parent and child with whatever you want to extract<br>
`kortex` - shorthand for `roslaunch kortex_driver kortex_driver.launch`<br>
`rospack find pkgname` - `package.xml` defines a package. Find if package "pkgname" package exists. Needed for launch files to find be able to create nodes in the package. Scripts and binaries sometimes need to be runnable `(sudo) chmod +x file`<br>
<br>
## Building programs
`source devel setup.bash` - needs to be done when we compile new programs using `catkin_make`<br>
`sws` - Petr Svarny's shorthand for the above<br>
`catkin_make` - makes the files<br>
`catkin clean` - cleans devel and runnables, a kind of reset<br>
`ipalm` - does sws and cd ~/vision_ws<br>
<br>
## Python scripts
Just run them as python scripts.<br>
`import ipdb; ipdb.set_trace()` - step-by-step debug from the line where this is 
<br>
## Conda & python
`deact` - deactivate conda, equivalent to `conda deactivate`<br>
`conda activate epos` - activate venv for the epos module ([original](https://github.com/thodan/epos_internal) and [ipalm](https://gitlab.fel.cvut.cz/body-schema/ipalm/epos_internal_robot3)).<br>
* Default Python version is 2.7.10, which is the one ROS runs on
* Epos and tensorflow 1.12-gpu runs on 3.6

`conda install` - only for venv

## Emergency git
`git status` - show status of files<br>
`git log` - show commit history. Up to date when origin/master and HEAD/master are on the same level.<br>
`git add .` - add all<br>
`git commit` - commit<br>
`git push origin master` - push commited changes to remote master.<br>
[Merge branch](https://stackoverflow.com/questions/5601931/what-is-the-best-and-safest-way-to-merge-a-git-branch-into-master)<br>
[Get one file](https://stackoverflow.com/questions/16230838/is-it-possible-to-pull-just-one-file-in-git) - usually `git fetch`, `git checkout` is enough.<br>
[Rebase](https://git-scm.com/docs/git-rebase) - apply changes of commit sha256 (= the code of the commit) to desired branch. E.g. when remote readme changed, to apply the changes offline.<br>
* E.g. `git rebase 06d12f6 master` - apply changes from `06d12f6` to master

## Emergency VIM
`:q` - quit<br>
`:q!` - quit without saving<br>
`:x` - save and quit<br>
`:w` - save (write)<br>
`:i` - go into edit mode<br>
`:u` - undo<br>
`/sth` - search for sth<br>
[Find same string](https://stackoverflow.com/questions/6607630/find-next-in-vim/6607664#6607664)<br>
[Replace text](https://www.cyberciti.biz/faq/vim-text-editor-find-and-replace-all-text/)<br>
[Basic commands](https://coderwall.com/p/adv71w/basic-vim-commands-for-getting-started)<br>
<br>
<br> 